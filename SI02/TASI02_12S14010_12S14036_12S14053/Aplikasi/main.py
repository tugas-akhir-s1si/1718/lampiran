import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D
from keras.utils import to_categorical

import librosa
import librosa.display
import os
from sklearn.model_selection import train_test_split
from keras.utils import to_categorical
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D,AveragePooling2D,ZeroPadding2D,BatchNormalization
from preprocess import *

import cnn_metode1 as metode1


## Data Path
DATA_PATH = "/home/daniel/data/"

## HYPER PARAMETER DARI CNN
feature_dim_1 = 13
channel = 1
epochs = 25
batch_size = 100
verbose = 1
num_classes = 20
feature_dim_2 = 11
	
def main():
	#get labels
	labels, _, _ = metode1.get_labels(DATA_PATH)

	# Save data to array file first
	metode1.save_data_to_array(DATA_PATH,max_len=feature_dim_2)

	# # Loading train set and test set
	X_train, X_test, y_train, y_test = metode1.get_train_test(DATA_PATH)
 
	# Reshaping to perform 2D convolution
	X_train = X_train.reshape(X_train.shape[0], feature_dim_1, feature_dim_2, channel)
	X_test = X_test.reshape(X_test.shape[0], feature_dim_1, feature_dim_2, channel)

	y_train_hot = to_categorical(y_train)
	y_test_hot = to_categorical(y_test)

	# Predicts one sample
	def predict(filepath, model):
	    sample = metode1.wav2mfcc(filepath)
	    sample_reshaped = sample.reshape(1, feature_dim_1, feature_dim_2, channel)
	    return get_labels()[0][
	            np.argmax(model.predict(sample_reshaped))
	    ]

	#Fit the model *you can chose the model that you want to fit by model that list on bottom line
	model = metode1.model_spnet4()
	model.fit(X_train, y_train_hot, batch_size=batch_size, epochs=epochs, verbose=verbose, validation_data=(X_test, y_test_hot))

	print(predict('/home/daniel/datatest/right/fb86d3c_nohash_0.wav', model=model))

if __name__ == "__main__":
	main()


# LIST FOR MODEL FUNCTION CNN :
# 1. model_lenet()
# 2. model_spnet()
# 3. model_spnet2()
# 4. model_spnet3()
# 5. model_spnet4()
# 6. model_spnet6()